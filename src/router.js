import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/auth/login/component.vue'
import Signup from './views/auth/signup/component.vue'
import App from './views/app/component.vue'
import Dashboard from './views/app/dashboard/component.vue'
import Settings from './views/app/settings/component.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/app',
      name: 'app',
      component: App,
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: 'settings',
          name: 'settings',
          component: Settings
        }
      ]
    }
  ]
})
